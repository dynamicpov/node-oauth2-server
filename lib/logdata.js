var _ = require('lodash');
var moment = require('moment');
var winston = require('../../logging/winston');

function logdata(message, message_type = 'info', line_number = 0) {
    let _message = {
        time: moment(new Date()).format("YYYYMMDDHHmmss"),
        detail: message
    };

    if (message_type === 'info') {
        winston.info(_message);
    }
    
    if (message_type === 'warn') {
        winston.warn(_message);
    }
    
    if (message_type === 'error') {
        winston.error(_message);
    }
    
    if (message_type === 'debug') {
        winston.debug(_message);
    }
}

module.exports = {
    logdata: logdata
}
    