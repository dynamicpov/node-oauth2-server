
/**
 * Module dependencies.
 */

var _ = require('lodash');
var AuthenticateHandler = require('./handlers/authenticate-handler');
var AuthorizeHandler = require('./handlers/authorize-handler');
var InvalidArgumentError = require('./errors/invalid-argument-error');
var TokenHandler = require('./handlers/token-handler');
const winston = require('winston');
var moment = require('moment');
/**
 * Constructor.
 */

function OAuth2Server(options) {
  options = options || {};

  const logger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [
      new winston.transports.File({ filename: '/var/log/node-oauth-server/error.log', level: 'error' }),
      new winston.transports.File({ filename: '/var/log/node-oauth-server/app.log' })
    ]
  });

  options.logger = {
    write_info(message, message_type = 'info', line_number = 0) {
      let _message = {
        time: moment(new Date()).format("YYYYMMDDHHmmss"),
        detail: message
      };

      if (message_type === 'info') {
        logger.info(_message);
      }
      
      if (message_type === 'warn') {
        logger.warn(_message);
      }
      
      if (message_type === 'error') {
        logger.error(_message);
      }
      
      if (message_type === 'debug') {
        logger.debug(_message);
      }
    }
  }

  if (!options.model) {
    throw new InvalidArgumentError('Missing parameter: `model`');
  }

  this.options = options;
}

/**
 * Authenticate a token.
 */

OAuth2Server.prototype.authenticate = function(request, response, options, callback) {
  options = _.assign({
    addAcceptedScopesHeader: true,
    addAuthorizedScopesHeader: true,
    allowBearerTokensInQueryString: false,
    ...this.options.logger
  }, this.options, options);
  
  return new AuthenticateHandler(options)
    .handle(request, response)
    .nodeify(callback);
};

/**
 * Authorize a request.
 */

OAuth2Server.prototype.authorize = function(request, response, options, callback) {
  options = _.assign({
    allowEmptyState: false,
    authorizationCodeLifetime: 60 * 60 * 24,
    ...this.options.logger
  }, this.options, options);
  return new AuthorizeHandler(options)
    .handle(request, response)
    .nodeify(callback);
};

/**
 * Create a token.
 */

OAuth2Server.prototype.token = function(request, response, options, callback) {
  options = _.assign({
    accessTokenLifetime: 60 * 60 * 24,
    refreshTokenLifetime: 60 * 60 * 24,
    ...this.options.logger
  }, this.options, options);
  return new TokenHandler(options)
    .handle(request, response)
    .nodeify(callback);
};

/**
 * Create a token.
 */

OAuth2Server.prototype.tokencode = function(request, response, options, callback) {
  options = _.assign({
    accessTokenLifetime: 60 * 60 * 24,
    refreshTokenLifetime: 60 * 60 * 24,
  }, this.options, options);

  return new TokenHandler(options)
    .handlecode(request, response)
    .nodeify(callback);
};

/**
 * Export constructor.
 */

module.exports = OAuth2Server;
