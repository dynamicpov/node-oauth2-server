
/**
 * Module dependencies.
 */

var AbstractGrantType = require('./abstract-grant-type');
var InvalidArgumentError = require('../errors/invalid-argument-error');
var InvalidGrantError = require('../errors/invalid-grant-error');
var Promise = require('bluebird');
var util = require('util');

/**
 * Constructor.
 */

function ClientCredentialsGrantType(options) {
  options = options || {};

  if (!options.model) {
    throw new InvalidArgumentError('Missing parameter: `model`');
  }

  if (!options.model.getUserFromClient) {
    throw new InvalidArgumentError('Invalid argument: model does not implement `getUserFromClient()`');
  }

  if (!options.model.saveToken) {
    throw new InvalidArgumentError('Invalid argument: model does not implement `saveToken()`');
  }

  AbstractGrantType.call(this, options);
}

/**
 * Inherit prototype.
 */

util.inherits(ClientCredentialsGrantType, AbstractGrantType);

/**
 * Handle client credentials grant.
 *
 * @see https://tools.ietf.org/html/rfc6749#section-4.4.2
 */

ClientCredentialsGrantType.prototype.handle = function(request, client) {
  if (!request) {
    throw new InvalidArgumentError('Missing parameter: `request`');
  }

  if (!client) {
    throw new InvalidArgumentError('Missing parameter: `client`');
  }

  var scope = this.getScope(request);
  var impersonatedUser = this.getUserToImpersonate(request);
  console.log("ClientCredentialsGrantType.prototype.handle.userToImpersonate: ", impersonatedUser);

  return Promise.bind(this)
    .then(function() {
      return this.getUserFromClient(client, impersonatedUser);
    })
    .then(function(user) {
      console.log("ClientCredentialsGrantType.prototype.handle.user: ", user);
      return this.saveToken(user, client, scope, impersonatedUser);
    });
};


/**
 * Get user to impersonate.
 */

ClientCredentialsGrantType.prototype.getUserToImpersonate = function(request) {
  var username = request.body.username || request.query.username;
  var profile = request.body.profile || request.query.profile;

  if (!username) {
    return false;
  }

  return {
    username: username,
    profile: profile
  };
};


/**
 * Get scope from the request.
 */

ClientCredentialsGrantType.prototype.getScope = function(request) {
  var scope = request.body.scope || request.query.scope;

  /*
  console.log("ClientCredentialsGrantType.prototype.getScope: ", scope);

  if (scope === undefined) {
    throw new InvalidScopeError('Invalid parameter: `scope`');
  }
  */

  return scope;
};


/**
 * Retrieve the user using client credentials.
 */

ClientCredentialsGrantType.prototype.getUserFromClient = function(client, impersonatedUser) {
  var fns = [
    this.model.getUserFromClient(client, impersonatedUser)
  ];

  return Promise.all(fns)
    .bind(this)
    .spread(function(user) {
      if (!user) {
        throw new InvalidGrantError('Invalid grant: user credentials are invalid');
      }

      console.log("ClientCredentialsGrantType.prototype.saveToken: ", user);
      return user;
    });
};

/**
 * Save token.
 */

ClientCredentialsGrantType.prototype.saveToken = function(user, client, scope, impersonated) {
  if (impersonated) {
    return this.saveImpersonatedToken(user, client, scope);
  }

  var fns = [
    this.generateAccessToken(),
    this.getAccessTokenExpiresAt()
  ];

  return Promise.all(fns)
    .bind(this)
    .spread(function(accessToken, accessTokenExpiresAt) {
      var token = {
        accessToken: accessToken,
        accessTokenExpiresAt: accessTokenExpiresAt,
        scope: scope
      };
      return this.model.saveToken(token, client, user);
    });
};

ClientCredentialsGrantType.prototype.saveImpersonatedToken = function(user, client, scope) {
  
  console.log("ClientCredentialsGrantType.prototype.saveImpersonatedToken");
  
  var fns = [
    this.generateAccessToken(),
    this.getAccessTokenExpiresAt(),
    this.generateRefreshToken(),
    this.getRefreshTokenExpiresAt()
  ];

  return Promise.all(fns)
    .bind(this)
    .spread(function(accessToken, accessTokenExpiresAt, refreshToken, refreshTokenExpiresAt) {
      var token = {
        accessToken: accessToken,
        accessTokenExpiresAt: accessTokenExpiresAt,
        refreshToken: refreshToken,
        refreshTokenExpiresAt: refreshTokenExpiresAt,
        scope: scope
      };

      return this.model.saveToken(token, client, user);
    });
};


/**
 * Export constructor.
 */

module.exports = ClientCredentialsGrantType;
